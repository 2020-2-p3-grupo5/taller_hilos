atmul: matriz.c
	gcc -Wall -fsanitize=address,undefined -pthread matriz.c -o matmul

.PHONY: clean
clean:
	rm matmul
